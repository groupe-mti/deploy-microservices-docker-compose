# Deploy microservices docker compose

The purpose of this repository is to create a docker compose used for testing and local deployement of the PLIC back-end project.

## Requirements

- Docker
- Docker-compose


## Usage

`docker-compose up` will start the containers along with their dapr sidecar.
It also starts a redis container for the dapr state store along with the dapr master.

Keep it at the same level of the plic-backend repository and TTS microservice repository.

```
        .
        ├── holo-conte-backend
        ├── holo-conte-tts
        └── deploy microservices docker compose
```

In order for the docker compose file to build the TTS microservice and main API images.

The .env file is also required at `../holo-conte-backend/HoloConteApi/.env` 

## License

LGPL-3.0

